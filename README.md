# Reading of free licences

## What's that !?
In this repository, I store the most common free licences in Markdown, I put them in shape (adding page breaks, defining titles level, list...), and I design the result with the help of TailwindCSS to add some typography basics. Then I do some HTML and PDF exports with the VSCode extension `Markdown PDF`... PDF are built to be printed but they can be read 'as is' as well.

## But why ?
First, I think it's important as free software developers and passionate users, to have read at least once all common free licences. Sometimes, we get a sense of what they mean, we read some parts and explanations, but we don't have a full understanding. I think that as we have to choose a specific licence for our programs, we have the responsability to fully understand what they include. We need to have at least a basic understanding of what is copyright, patents, trademarks, and public domain. Then, with this legal knowledge we can confidently copy code and sublicence some the code, notes, documentations and scripts we produce over the time.

*PS: not all common licences are present for the moment... Do you want to add them ? Send a few PR !*

## How to add and export licenses as PDF ?
*First disclaimer: this a rudimentary way of doing custom designed PDF but I don't have a better way to do it for now...*

Clone and load npm dependencies by running `npm install`. Run `npm run tailwindbuild` or `npm run tailwindwatch` to generate `build.css`, you can customize the style via `design.css` or leave the default one. Make sure you have the font Fira Code installed.

**For each license:**
1. Get the raw text of the license from their official website
1. Commit the text in a file under `licences/` under their SPDX identifier if possible like `AGPL-3.0.md` (simplified version because `AGPL-3.0-OR-LATER` has the same text than `AGPL-3.0-ONLY`).
1. Transform headings in the text to Markdown titles: `##`, `###` ..., add double space for simple line breaks when necessary. (Or just enable line breaks in Markdown PDF settings).
1. Adapt the licence name in `.vscode/settings.json` under `markdown-pdf.headerTemplate`. Export in PDF with Markdown PDF in VSCode. You will get a PDF next to the Markdown file.
1. Commit the adapted text with headings and the PDF version
1. Send a PR :)

## Default style
- Fira Code
- A few margins, paddings, headings size, very hand crafted.
- A few settings for the PDF rendering (document margins)

Change `design.css` if you don't like it. Maybe a switch to Tailwind Typography plugin could be way better...

## List of licences
### Permissive
- Apache License 2.0 (Apache-2.0)
- 3-clause BSD license (BSD-3-Clause)
- 2-clause BSD license (BSD-2-Clause)
- MIT license (MIT)
- The Unlicense (Unlicense)
- SIL Open Font License 1.1 (OFL-1.1)
- ISC License (ISC)

### Copyleft
- GNU General Public License (GPL)
- GNU Lesser General Public License (LGPL)
- GNU Affero General Public License version 3 (AGPL-3.0)
- Mozilla Public License 2.0 (MPL-2.0)

### Others
- Common Development and Distribution License 1.0 (CDDL-1.0)
- Eclipse Public License 2.0 (EPL-2.0)

### Creative commons suite
- CC BY 4.0
- CC BY-SA 4.0
- CC BY-NC 4.0
- CC BY-NC-ND 4.0
- CC0 1.0

### Non free (but used a bit)
- WTFPL v1 and v2
- Beerware license

## Licence
Any code and notes in this repository, excluding the `licenses` folder, is released under the [MIT licence](LICENSE). Thus, any contribution to this repos is released under the same licence. This obviously doesn't concern the licences text themself.